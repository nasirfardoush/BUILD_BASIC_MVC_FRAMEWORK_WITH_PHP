<?php

// Define DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "db_mvc");

// Define URL
define("ROOT_PATH", "/phpmvc.dev/");
define("ROOT_URL", "http://localhost/phpmvc.dev/");